/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.lib.Constants.Language;

/**
 * A fully detailed MDR Element:
 *
 * <pre>
 * - contains all members
 * - contains all slots
 * - contains all versions
 * - contains the value domain
 * </pre>
 *
 * @author paul
 *
 */
public class DetailedMDRElement extends MDRElement {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * A list of members of this element. This list if empty for
     * data elements.
     */
    private List<MDRElement> members;

    /**
     * A list of slots for this element.
     */
    private List<SlotDTO> slots;

    /**
     * A list of Scoped Identifiers of this element in different versions.
     */
    private List<ScopedIdentifier> versions;

    /**
     * The value domain for this element. Null if this element is not a data element.
     */
    private ValueDomainDTO valueDomain;

    public DetailedMDRElement(IdentifiedElement element, Language priority) {
        super(element, priority);
    }

    public List<MDRElement> getMembers() {
        return members;
    }

    public void setMembers(List<MDRElement> members) {
        this.members = members;
    }

    public List<SlotDTO> getSlots() {
        return slots;
    }

    public void setSlots(List<SlotDTO> slots) {
        this.slots = slots;
    }

    public List<ScopedIdentifier> getVersions() {
        return versions;
    }

    public void setVersions(List<ScopedIdentifier> versions) {
        this.versions = versions;

        /**
         * try to sort the versions as numbers.
         */
        Collections.sort(versions, new Comparator<ScopedIdentifier>() {
            @Override
            public int compare(ScopedIdentifier o1, ScopedIdentifier o2) {
                try {
                    return Float.compare(Float.parseFloat(o1.getVersion()), Float.parseFloat(o2.getVersion()));
                } catch(Exception e) {
                    return 0;
                }
            }
        });
    }

    public ValueDomainDTO getValueDomain() {
        return valueDomain;
    }

    public void setValueDomain(ValueDomainDTO valueDomain) {
        this.valueDomain = valueDomain;
    }

}
