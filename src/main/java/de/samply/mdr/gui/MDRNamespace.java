/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui;

import java.util.Comparator;
import java.util.List;

import de.samply.mdr.dal.dto.DescribedElement;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.util.Mapper;

/**
 * A namespace from the mdr with all definitions.
 * Shows a "prioritized" language.
 * @author paul
 *
 */
public class MDRNamespace extends Namespace {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The prioritized language
     */
    private Language priority;

    /**
     * The list of definitions for this namespace.
     */
    private List<AdapterDefinition> definitions;

    /**
     * Creates a new namespace from the given namespace.
     * @param namespace
     * @param priority
     */
    public MDRNamespace(DescribedElement namespace, Language priority) {
        Namespace ns = (Namespace) namespace.getElement();

        setCreatedBy(ns.getCreatedBy());
        setId(ns.getId());
        setName(ns.getName());
        setData(ns.getData());

        definitions = Mapper.convert(namespace.getDefinitions());
        this.priority = priority;
    }

    public String getDefinition() {
        return getPrioritizedDefinition().getDefinition();
    }

    public String getDesignation() {
        return getPrioritizedDefinition().getDesignation();
    }

    public AdapterDefinition getPrioritizedDefinition() {
        for(AdapterDefinition d : definitions) {
            if(d.getLanguage() == priority) {
                return d;
            }
        }
        return definitions.get(0);
    }

    public String getLabel() {
        return getName() + " : " + getPrioritizedDefinition().getDesignation() + ", " + getPrioritizedDefinition().getDefinition();
    }

    public String getShortLabel() {
        return getName() + " : " + getPrioritizedDefinition().getDesignation();
    }

    /* (non-Javadoc)
     * @see de.samply.mdr.dal.dto.Namespace#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof MDRNamespace)) {
            return false;
        }

        MDRNamespace other = (MDRNamespace) obj;

        return(other.getName().equals(getName()));
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    public static final Comparator<MDRNamespace> nameComparator = new Comparator<MDRNamespace>() {
        @Override
        public int compare(MDRNamespace arg0, MDRNamespace arg1) {
            return arg0.getDesignation().compareTo(arg1.getDesignation());
        }
    };

}
