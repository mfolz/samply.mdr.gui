/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.utils.Importer;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.MDRNamespace;
import de.samply.sdao.DAOException;

/**
 * Handles the import of one element into another namespace
 * @author paul
 *
 */
@ManagedBean
@ViewScoped
public class ImportBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty("#{userBean}")
    private UserBean userBean;

    /**
     * imports the given element into the given namespace.
     * @param element
     * @param namespace
     * @return
     */
    public String importElement(MDRElement element, MDRNamespace namespace) {
        Importer importer = new Importer();

        /**
         * If the namespace is not writable, abort without an error message. The user tried to do something he
         * shouldn't be able to do anyway.
         */
        if(!userBean.isWritableNamespace(namespace)) {
            return null;
        }

        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            String redirect = "/detail.xhtml?faces-redirect=true&urn="
                    + importer.importElement(mdr, element.getUrn(),	namespace.getName(), userBean.getUserId());

            mdr.commit();
            return redirect;
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

}
