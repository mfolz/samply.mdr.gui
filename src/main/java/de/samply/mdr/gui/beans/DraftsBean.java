/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;

/**
 * Loads all drafts
 * @author paul
 *
 */
@ManagedBean
@ViewScoped
public class DraftsBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * The list of elements that are currently in the draft status.
     */
    private List<MDRElement> elements = Collections.emptyList();

    /**
     * the list of elements that have been marked for release.
     */
    private HashMap<String, MDRElement> marked = new HashMap<>();

    /**
     * The urn of the element that will be marked
     */
    private String markItem;

    /**
     * Called by the f:viewAction
     */
    public void initialize() {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            elements = Mapper.convertElements(mdr.get(IdentifiedDAO.class).getDrafts(),
                    getUserBean().getSelectedLanguage(), true);
            marked = new HashMap<>();
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    public void toggle() {
        if(marked.containsKey(markItem)) {
            marked.remove(markItem);
        } else {
            try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
                marked.put(markItem, Mapper.convert(mdr.get(IdentifiedDAO.class).getElement(markItem),
                        getUserBean().getSelectedLanguage()));
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
    }

    public String release() {
        try(MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            for(MDRElement element : marked.values()) {
                mdr.get(ScopedIdentifierDAO.class).release(element.getElement().getScoped());
            }
            mdr.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        initialize();
        return null;
    }

    public Boolean isMarked(MDRElement element) {
        return marked.containsKey(element.getUrn());
    }

    public List<MDRElement> getElements() {
        return elements;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public void setElements(List<MDRElement> elements) {
        this.elements = elements;
    }

    public String getMarkItem() {
        return markItem;
    }

    public void setMarkItem(String markUrn) {
        this.markItem = markUrn;
    }

    public HashMap<String, MDRElement> getMarked() {
        return marked;
    }

    public void setMarked(HashMap<String, MDRElement> marked) {
        this.marked = marked;
    }

}
