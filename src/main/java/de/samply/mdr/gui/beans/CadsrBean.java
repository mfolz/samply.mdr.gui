/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.ClientBuilder;

import de.samply.cadsr.CadsrClient;
import de.samply.cadsr.CadsrElement;

@ManagedBean
@ViewScoped
public class CadsrBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String publicId;

    private CadsrElement element;

    public void initialize() {
        CadsrClient client = new CadsrClient(ClientBuilder.newClient());
        try {
            element = client.getElement(publicId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the publicId
     */
    public String getPublicId() {
        return publicId;
    }

    /**
     * @param publicId the publicId to set
     */
    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    /**
     * @return the element
     */
    public CadsrElement getElement() {
        return element;
    }

    /**
     * @param element the element to set
     */
    public void setElement(CadsrElement element) {
        this.element = element;
    }


}
