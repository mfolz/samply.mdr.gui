/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.utils.Updater;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;

/**
 * A bean that shows the update process of a single group
 */
@ManagedBean
@ViewScoped
public class UpdateBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;

    /**
     * The URN that will be updated with this bean
     */
    private String urn;

    /**
     * The replacement map.
     */
    private Map<MDRElement, MDRElement> replacements;

    /**
     * The list of updated elements
     */
    private List<MDRElement> keys;

    /**
     * The element that will be updated with this bean
     */
    private MDRElement element;

    /**
     * Called by an ajax request. It *initializes*
     * the maps, but does not commit the SQL connection, so this process
     * does not update the database in any way. The maps are used to show the user exactly
     * what will be updated by this process.
     * @return
     */
    public String initialize() {
        if(! userBean.isWritableNamespaceURN(urn)) {
            return "/detail?faces-redirect=true&urn=" + urn;
        }

        try (MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            element = Mapper.convert(mdr.get(IdentifiedDAO.class).getElement(urn), userBean.getSelectedLanguage());

            Updater updater = new Updater(mdr, urn, userBean.getUserId());
            updater.update();

            this.replacements = new HashMap<>();

            Map<IdentifiedElement, IdentifiedElement> replacements = updater.getRootReplacements();

            for(Entry<IdentifiedElement, IdentifiedElement> entry : replacements.entrySet()) {
                if(entry.getValue() == null) {
                    this.replacements.put(Mapper.convert(entry.getKey(), userBean.getSelectedLanguage()), null);
                } else {
                    this.replacements.put(Mapper.convert(entry.getKey(), userBean.getSelectedLanguage()),
                            Mapper.convert(entry.getValue(), userBean.getSelectedLanguage()));
                }
            }
            keys = new ArrayList<>();
            keys.addAll(this.replacements.keySet());
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Executes the real update and redirects the user to the updated element.
     * detail page.
     * @return
     */
    public String update() {
        if(! userBean.isWritableNamespaceURN(urn)) {
            return "/detail?faces-redirect=true&urn=" + urn;
        }

        try (MDRConnection mdr = ConnectionFactory.get(getUserBean().getUserId())) {
            Updater updater = new Updater(mdr, urn, userBean.getUserId());
            IdentifiedElement newElement = updater.update();
            mdr.commit();

            return "/detail?faces-redirect=true&urn=" + newElement.getScoped().toString();
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Returns the status of the given element.
     * @param element
     * @return
     */
    public String status(MDRElement element) {
        if(! keys.contains(element)) {
            /**
             * FYI: this doesnt make sense...
             */
            return "warning";
        } else if(isDeleted(element)) {
            return "danger";
        } else {
            return "success";
        }
    }

    /**
     * Returns true if the element will be removed from the group
     * @param element
     * @return
     */
    public Boolean isDeleted(MDRElement element) {
        return keys.contains(element) && replacements.get(element) == null;
    }

    /**
     * @return the urn
     */
    public String getUrn() {
        return urn;
    }

    /**
     * @param urn the urn to set
     */
    public void setUrn(String urn) {
        this.urn = urn;
    }

    /**
     * @return the userBean
     */
    public UserBean getUserBean() {
        return userBean;
    }

    /**
     * @param userBean the userBean to set
     */
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    /**
     * @return the replacements
     */
    public Map<MDRElement, MDRElement> getReplacements() {
        return replacements;
    }

    /**
     * @param replacements the replacements to set
     */
    public void setReplacements(Map<MDRElement, MDRElement> replacements) {
        this.replacements = replacements;
    }

    /**
     * @return the keys
     */
    public List<MDRElement> getKeys() {
        return keys;
    }

    /**
     * @param keys the keys to set
     */
    public void setKeys(List<MDRElement> keys) {
        this.keys = keys;
    }

    /**
     * @return the element
     */
    public MDRElement getElement() {
        return element;
    }

    /**
     * @param element the element to set
     */
    public void setElement(MDRElement element) {
        this.element = element;
    }

}
