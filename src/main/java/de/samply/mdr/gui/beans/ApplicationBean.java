/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import de.samply.mdr.gui.MDRConfig;

/**
 * The application wide bean that is used to get the maven version of this
 * application.
 * @author paul
 *
 */
@ManagedBean
@ApplicationScoped
public class ApplicationBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The version of this application. It is read out only once.
     */
    private String version = null;

    /**
     * Returns the maven version of this application.
     * @return
     */
    public String getVersion() {
        if (version == null) {
            version  = getClass().getPackage().getImplementationVersion();
            if (version == null) {
                Properties prop = new Properties();
                try {
                    prop.load(FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/META-INF/MANIFEST.MF"));
                    version = prop.getProperty("Implementation-Version");
                } catch (IOException e) {
                }
            }
        }
        return version;
    }

    /**
     * Returns the current database version
     * @return
     */
    public String getDBVersion() {
        return "" + MDRConfig.getDbVersion();
    }

    /**
     * Returns the JSF version of the currently used JSF library
     * @return
     */
    public String getJSFVersion() {
        return FacesContext.class.getPackage().getImplementationVersion();
    }

    /**
     * Returns the JSF vendor of the currently used JSF library
     * @return
     */
    public String getJSFTitle() {
        return FacesContext.class.getPackage().getImplementationTitle();
    }

    /**
     * Should an automatic translation button be offered for definitions
     *
     * @return true if the button shall be shown
     */
    public Boolean showTranslateDefinitionButton() {
        return MDRConfig.isShowTranslateDefinitionButton();
    }

}
