/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui.beans;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import de.samply.auth.client.AuthClient;
import de.samply.auth.client.InvalidKeyException;
import de.samply.auth.client.InvalidTokenException;
import de.samply.auth.client.jwt.JWTAccessToken;
import de.samply.auth.client.jwt.JWTIDToken;
import de.samply.auth.client.jwt.JWTRefreshToken;
import de.samply.auth.client.jwt.JWTVocabulary;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.rest.Scope;
import de.samply.auth.rest.Usertype;
import de.samply.auth.utils.OAuth2ClientConfig;
import de.samply.common.config.OAuth2Client;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.User;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.NamespaceDAO;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.mdr.dao.SlotDAO;
import de.samply.mdr.dao.UserDAO;
import de.samply.mdr.gui.ClientUtil;
import de.samply.mdr.gui.ConnectionFactory;
import de.samply.mdr.gui.DetailedMDRElement;
import de.samply.mdr.gui.MDRConfig;
import de.samply.mdr.gui.MDRElement;
import de.samply.mdr.gui.MDRNamespace;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.Mapper;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;

/**
 * This session scoped bean stores all user informations:
 * <pre>
 * - his id in the database
 * - his username and email
 * - the language he selected, currently always german
 * - the namespaces that he can read and write
 * - the elements that he starred or wants to compare
 * </pre>
 * @author paul
 *
 */
@ManagedBean
@SessionScoped
public class UserBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The current username (email). Null if the login is not valid
     */
    private String username = null;

    /**
     * The current real name. Null if the login is not valid.
     */
    private String realName = null;

    /**
     * The current user identity (usually a URL that identifies the user). Null if the login is not valid.
     */
    private String userIdentity = null;

    /**
     * If the login is valid this value is true. False otherwise.
     */
    private Boolean loginValid = false;

    /**
     * The *mapped* user ID in the database.
     */
    private int userId = 0;

    /**
     * The currently selected language.
     */
    private Language selectedLanguage = Language.DE;

    /**
     * The currently starred MDR elements
     */
    private LinkedList<MDRElement> starred = new LinkedList<>();

    /**
     * The currently selected elements, that the user wants to compare. Usually this is a list of groups.
     */
    private LinkedList<MDRElement> compare = new LinkedList<>();

    /**
     * The currently for export selected elements.
     */
    private ArrayList<MDRElement> export = new ArrayList<>();

    /**
     * The currently for export selected namespaces
     */
    private ArrayList<String> exportNamespace = new ArrayList<>();

    /**
     * The list of currently writable namespaces.
     */
    private List<MDRNamespace> writableNamespaces;

    /**
     * The list of currently readable namespaces.
     */
    private List<MDRNamespace> readableNamespaces;

    /**
     * The list of currently readable namespaces, that are not explicitly readable (e.g. they are public)
     */
    private List<MDRNamespace> otherReadableNamespaces;

    /**
     * The URN of the item that is about to get starred.
     */
    private String starItem;

    /**
     * If true the current user is able to create new namespaces.
     */
    private Boolean canCreateNamespace;

    /**
     * If the the current user is able to create new namespaces.
     */
    private Boolean canCreateCatalogs;

    /**
     * If true the user can use the export and import
     */
    private Boolean canExportImport;

    /**
     * The currently selected namespace for this user.
     */
    private MDRNamespace selectedNamespace = null;

    /**
     * The state of this session. This is a random string for OAuth2 used to prevent cross site forgery attacks.
     */
    private String state;

    /**
     * The JWTs from OAuth2
     */
    private JWTAccessToken accessToken;

    /**
     * The JWT Open ID token
     */
    private JWTIDToken idToken;

    /**
     * The JWT refresh token
     */
    private JWTRefreshToken refreshToken;


    /**
     * Compares two namespaces by their names
     */
    private static final Comparator<MDRNamespace> namespaceComparator = new Comparator<MDRNamespace>() {
        @Override
        public int compare(MDRNamespace o1, MDRNamespace o2) {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    };

    /**
     * Initializes the bean: loads all accessible namespaces.
     */
    @PostConstruct
    public void init() {
        state = new BigInteger(64, new SecureRandom()).toString(32);

        try(MDRConnection mdr = ConnectionFactory.get(userId)) {
            reloadNamespaces(mdr);
        } catch (DAOException e) {
            e.printStackTrace();

            readableNamespaces = Collections.emptyList();
            writableNamespaces = Collections.emptyList();
        }

        try {
            selectedLanguage = Language.valueOf(FacesContext.getCurrentInstance()
                    .getExternalContext().getRequestLocale().getLanguage().toUpperCase());
        } catch(Exception e) {
            selectedLanguage = Language.EN;
        }
    }

    /**
     * Returns true, if this element is currently starred
     * @param element
     * @return
     */
    public Boolean isStarred(MDRElement element) {
        return starred.contains(element);
    }

    /**
     * Toggles the starred element
     * @param element
     * @return
     */
    public String toggle(MDRElement element) {
        if(starred.contains(element)) {
            starred.remove(element);
        } else {
            starred.addFirst(element);
        }
        return null;
    }

    /**
     * Starres the given element
     * @param element
     */
    public void star(MDRElement element) {
        /**
         * Make sure that all other elements with the same URN are unstarred
         * This happens when drafts are saved
         */
        Iterator<MDRElement> iterator = starred.iterator();
        while(iterator.hasNext()) {
            if(iterator.next().getUrn().equals(element.getUrn())) {
                iterator.remove();
            }
        }

        starred.addFirst(element);
    }

    /**
     * Toggles the star on the "starItem"
     * @return
     */
    public String toggle() {
        Iterator<MDRElement> iterator = starred.iterator();

        while(iterator.hasNext()) {
            MDRElement element = iterator.next();
            if(element.getUrn().equals(starItem)) {
                iterator.remove();
                return null;
            }
        }

        try(MDRConnection mdr = ConnectionFactory.get(userId)) {
            starred.addFirst(Mapper.convert(mdr.get(IdentifiedDAO.class).getElement(starItem), getSelectedLanguage()));
        } catch (DAOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Gets the fully described element from the database.
     * With slots, its members, all available versions
     *
     * @param urn
     * @return
     */
    public DetailedMDRElement getDetailMDRElement(String urn) {
        try(MDRConnection mdr = ConnectionFactory.get(userId)) {
            return Mapper.convert(mdr.get(IdentifiedDAO.class).getElement(urn), Mapper.convertSlots(mdr.get(SlotDAO.class).getSlots(urn)),
                    mdr.get(ScopedIdentifierDAO.class).getVersions(urn), mdr, getSelectedLanguage());
        } catch (DAOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Outdates the given element (Sets the status of the scoped identifier to outdated).
     * @param element
     * @return
     */
    public String deleteElement(MDRElement element) {
        if(! isWritableNamespaceURN(element.getUrn())) {
            return null;
        }

        try(MDRConnection mdr = ConnectionFactory.get(userId)) {
            mdr.get(ScopedIdentifierDAO.class).outdateIdentifier(element.getUrn());
            mdr.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return "detail.xhtml?urn=" + element.getUrn() + "&faces-redirect=true";
    }

    /**
     * Lets the user login and sets all necessary fields
     *
     * @param accessToken
     * @param idToken
     * @throws InvalidKeyException
     * @throws InvalidTokenException
     */
    public void login(AuthClient client) throws InvalidTokenException, InvalidKeyException {
        accessToken = client.getAccessToken();
        idToken = client.getIDToken();
        refreshToken = client.getRefreshToken();

        /**
         * Make sure that if the access token contains a state parameter, that it matches the state variable.
         * If it does not, abort.
         */
        if(!StringUtil.isEmpty(accessToken.getState()) && !state.equals(accessToken.getState())) {
            accessToken = null;
            idToken = null;
            refreshToken = null;
            return;
        }

        loginValid = true;
        userIdentity = client.getIDToken().getSubject();
        realName = client.getIDToken().getName();
        username = client.getIDToken().getEmail();
        starred = new LinkedList<>();
        compare = new LinkedList<>();
        createOrGetUser(client.getIDToken());
    }

    /**
     * If the "userIdentity" does not exist in the database, create it.
     */
    private void createOrGetUser(JWTIDToken idToken) {
        try(MDRConnection mdr = ConnectionFactory.get(userId)) {
            User user = mdr.get(UserDAO.class).getUserByIdentity(userIdentity);
            if(user == null) {
                user = mdr.get(UserDAO.class).createDefaultUser(idToken.getSubject(), idToken.getEmail(), idToken.getName(),
                        idToken.getExternalLabel(), MDRConfig.getNamespaceGrant(), MDRConfig.getCatalogsGrant(), MDRConfig.getExportImportGrant());

                userId = user.getId();
            } else {
                /**
                 * Update his email.
                 */
                userId = user.getId();

                user.setEmail(username);
                user.setRealName(idToken.getName());
                user.setExternalLabel(idToken.getExternalLabel());

                mdr.get(UserDAO.class).updateUser(user);
            }
            canCreateNamespace = user.getCanCreateNamespace();
            canCreateCatalogs = user.getCanCreateCatalog();
            canExportImport = user.getCanExportImport();

            mdr.updateUserId(userId);

            reloadNamespaces(mdr);

            @SuppressWarnings("unchecked")
            List<String> descriptions = (List<String>) idToken.getClaimsSet().getClaim(JWTVocabulary.DESCRIPTION);

            /**
             * If the user has a registry, he has a public key known in Samply Auth. If he does,
             * the array of descriptions is not empty. In this case, automatically create a
             * namespace with the "osse-" prefix.
             * TODO: This might be problematic, since Samply Auth 1.4 the user can add public keys and then
             * he gets a new namespace in the MDR.
             */
            if((idToken.getUsertype() == Usertype.OSSE_REGISTRY || idToken.getUsertype() == Usertype.BRIDGEHEAD)
                    && descriptions.size() > 0 && writableNamespaces.size() == 0) {
                int i = 1;
                String input = "osse-";
                Namespace namespace = mdr.get(ElementDAO.class).getNamespace(input + i);

                if(namespace != null) {
                    while(namespace != null) {
                        ++i;
                        namespace = mdr.get(ElementDAO.class).getNamespace(input + i);
                    }
                }

                input = input + i;

                Namespace ns = new Namespace();
                ns.setName(input);
                ns.setHidden(false);
                ns.setCreatedBy(user.getId());

                mdr.get(ElementDAO.class).saveElement(ns);

                Definition def = new Definition();
                def.setLanguage("en");
                def.setDefinition(descriptions.get(0));
                def.setDesignation(descriptions.get(0));
                def.setElementId(ns.getId());

                mdr.get(DefinitionDAO.class).saveNamespaceDefinition(def);

                /**
                 * Reload the namespaces once again...
                 */
                reloadNamespaces(mdr);
            }

            if(writableNamespaces.size() > 0) {
                selectedNamespace = writableNamespaces.get(0);
            }

            mdr.commit();
        } catch(DAOException e) {
            e.printStackTrace();
        }

        try {
            selectedLanguage = Language.valueOf(FacesContext.getCurrentInstance()
                    .getExternalContext().getRequestLocale().getLanguage().toUpperCase());
        } catch(Exception e) {
            selectedLanguage = Language.EN;
        }
    }

    /**
     * This step uses the readable and writable namespaces
     * to separate them into two distinct groups. Also it sorts them.
     */
    private void updateOtherNamespaces() {

        HashSet<String> writable = new HashSet<>();
        otherReadableNamespaces = new ArrayList<>();

        for(MDRNamespace ns : writableNamespaces) {
            writable.add(ns.getName());
        }

        for(MDRNamespace ns : readableNamespaces) {
            if(!writable.contains(ns.getName())) {
                otherReadableNamespaces.add(ns);
            }
        }

        Collections.sort(otherReadableNamespaces, namespaceComparator);
        Collections.sort(writableNamespaces, namespaceComparator);
    }

    /**
     * Reloads the readable and writable namespaces and updates the internal lists.
     * @param mdr
     * @throws DAOException
     */
    public void reloadNamespaces(MDRConnection mdr) throws DAOException {
        readableNamespaces = Mapper.convertNamespaces(mdr.get(NamespaceDAO.class).getReadableNamespaces(), selectedLanguage);
        writableNamespaces = Mapper.convertNamespaces(mdr.get(NamespaceDAO.class).getWritableNamespaces(), selectedLanguage);

        updateOtherNamespaces();

        if(writableNamespaces.size() > 0 && selectedNamespace == null) {
            selectedNamespace = writableNamespaces.get(0);
        }
    }

    /**
     * Executes a logout
     * @return
     * @throws IOException
     */
    public void logout() throws IOException {
        username = null;
        realName = null;
        loginValid = false;
        userIdentity = null;
        userId = 0;
        starred = new LinkedList<>();
        compare = new LinkedList<>();

        canCreateNamespace = false;
        canCreateCatalogs = false;

        accessToken = null;
        refreshToken = null;
        idToken = null;

        try(MDRConnection mdr = ConnectionFactory.get(userId)) {
            reloadNamespaces(mdr);
        } catch (DAOException e) {
            e.printStackTrace();
        }

        OAuth2Client config = MDRConfig.getOauth2();
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        context.redirect(OAuth2ClientConfig.getLogoutUrl(config, context.getRequestScheme(),
                context.getRequestServerName(), context.getRequestServerPort(),
                context.getRequestContextPath(), "/"));
    }

    /**
     * Checks if the given urn is in the list of writable namespaces
     * @param urn
     * @return
     */
    public Boolean isWritableNamespaceURN(String urn) {
        ScopedIdentifier scoped = ScopedIdentifier.fromURN(urn);
        return isWritableNamespaceByName(scoped.getNamespace());
    }

    /**
     * Checks if the given namespace is writable
     * @param namespace
     * @return
     */
    public Boolean isWritableNamespace(MDRNamespace namespace) {
        if(namespace == null) {
            return false;
        } else {
            return isWritableNamespaceByName(namespace.getName());
        }
    }

    /**
     * Checks if the given namespace is writable
     * @param namespace
     * @return
     */
    public Boolean isWritableNamespaceByName(String namespace) {
        for(MDRNamespace ns : getWritableNamespaces()) {
            if(ns.getName().equals(namespace)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the URL for Samply.Auth
     * @return
     * @throws UnsupportedEncodingException
     */
    public String getAuthenticationUrl() throws UnsupportedEncodingException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest req = (HttpServletRequest) context.getRequest();

        StringBuffer requestURL = new StringBuffer(context.getRequestServletPath());
        if (req.getQueryString() != null) {
            requestURL.append("?").append(req.getQueryString());
        }

        return OAuth2ClientConfig.getRedirectUrl(MDRConfig.getOauth2(), context.getRequestScheme(),
                context.getRequestServerName(), context.getRequestServerPort(),
                context.getRequestContextPath(), requestURL.toString(), null, state, Scope.OPENID);
    }

    /**
     * Returns a list of namespaces, that the given element can be imported to
     * @param element
     * @return
     */
    public List<MDRNamespace> getImportableNamespaces(MDRElement element) {
        List<MDRNamespace> target = new ArrayList<>();
        for(MDRNamespace ns : writableNamespaces) {
            if(!ns.getName().equals(element.getElement().getScoped().getNamespace())) {
                target.add(ns);
            }
        }
        return target;
    }

    /**
     * Adds the given element to the list of elements that the user
     * wants to compare.
     * @param element
     * @return
     */
    public String markToCompare(MDRElement element) {
        if(!compare.contains(element)) {
            compare.addFirst(element);
            return null;
        } else {
            return "/compare?faces-redirect=true";
        }
    }

    public String markToExport(MDRElement element) {
        if(!export.contains(element)) {
            export.add(element);
        }
        return null;
    }

    public String markToExport(String namespace) {
        if(!exportNamespace.contains(namespace)) {
            exportNamespace.add(namespace);
        }
        return null;
    }

    public String clearCompare() {
        compare.clear();
        return "/index?faces-redirect=true";
    }

    public String clearExport() {
        export.clear();
        exportNamespace.clear();
        return "/index?faces-redirect=true";
    }

    /**
     * @return a valid auth client with the current access tokens
     */
    public AuthClient getAuthClient() {
        OAuth2Client config = MDRConfig.getOauth2();
        return new AuthClient(config.getHost(), KeyLoader.loadKey(config.getHostPublicKey()), accessToken,
                idToken, refreshToken, ClientUtil.getClient());
    }

    public Boolean isExported(MDRElement element) {
        return export.contains(element);
    }

    public Boolean isExportedNamespace(String namespace) {
        return exportNamespace.contains(namespace);
    }

    public void selectNamespace(MDRNamespace namespace) {
        this.selectedNamespace = namespace;
    }

    public Boolean isSelected(MDRNamespace namespace) {
        return this.selectedNamespace != null && this.selectedNamespace.getName().equals(namespace.getName());
    }

    public Boolean isCompared(MDRElement element) {
        return compare.contains(element);
    }

    public List<MDRNamespace> getWritableNamespaces() {
        return writableNamespaces;
    }

    public List<MDRNamespace> getReadableNamespaces() {
        return readableNamespaces;
    }

    public Boolean getLoginValid() {
        return loginValid;
    }

    public void setLoginValid(Boolean loginValid) {
        this.loginValid = loginValid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Language getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(Language selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public void setWritableNamespaces(List<MDRNamespace> writableNamespaces) {
        this.writableNamespaces = writableNamespaces;
    }

    public void setReadableNamespaces(List<MDRNamespace> readableNamespaces) {
        this.readableNamespaces = readableNamespaces;
    }

    public String getStarItem() {
        return starItem;
    }

    public void setStarItem(String starItem) {
        this.starItem = starItem;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<MDRElement> getStarred() {
        return starred;
    }

    public LinkedList<MDRElement> getCompare() {
        return compare;
    }

    public void setCompare(LinkedList<MDRElement> compare) {
        this.compare = compare;
    }

    /**
     * @return the canCreateNamespace
     */
    public Boolean getCanCreateNamespace() {
        return canCreateNamespace;
    }

    /**
     * @param canCreateNamespace the canCreateNamespace to set
     */
    public void setCanCreateNamespace(Boolean canCreateNamespace) {
        this.canCreateNamespace = canCreateNamespace;
    }

    public List<MDRNamespace> getOtherReadableNamespaces() {
        return otherReadableNamespaces;
    }

    public void setOtherReadableNamespaces(List<MDRNamespace> otherReadableNamespaces) {
        this.otherReadableNamespaces = otherReadableNamespaces;
    }

    /**
     * @return the realName
     */
    public String getRealName() {
        return realName;
    }

    /**
     * @param realName the realName to set
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * @return the selectedNamespace
     */
    public MDRNamespace getSelectedNamespace() {
        return selectedNamespace;
    }

    /**
     * @param selectedNamespace the selectedNamespace to set
     */
    public void setSelectedNamespace(MDRNamespace selectedNamespace) {
        this.selectedNamespace = selectedNamespace;
    }

    /**
     * @return the accessToken
     */
    public JWTAccessToken getAccessToken() {
        return accessToken;
    }

    /**
     * @param accessToken the accessToken to set
     */
    public void setAccessToken(JWTAccessToken accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * @return the idtoken
     */
    public JWTIDToken getIdToken() {
        return idToken;
    }

    /**
     * @param idtoken the idtoken to set
     */
    public void setIdToken(JWTIDToken idtoken) {
        this.idToken = idtoken;
    }

    /**
     * @return the refreshToken
     */
    public JWTRefreshToken getRefreshToken() {
        return refreshToken;
    }

    /**
     * @param refreshToken the refreshToken to set
     */
    public void setRefreshToken(JWTRefreshToken refreshToken) {
        this.refreshToken = refreshToken;
    }

    /**
     * @return the canCreateCatalogs
     */
    public Boolean getCanCreateCatalogs() {
        return canCreateCatalogs;
    }

    /**
     * @param canCreateCatalogs the canCreateCatalogs to set
     */
    public void setCanCreateCatalogs(Boolean canCreateCatalogs) {
        this.canCreateCatalogs = canCreateCatalogs;
    }

    /**
     * @return the export
     */
    public ArrayList<MDRElement> getExport() {
        return export;
    }

    /**
     * @param export the export to set
     */
    public void setExport(ArrayList<MDRElement> export) {
        this.export = export;
    }

    /**
     * @return the exportNamespace
     */
    public ArrayList<String> getExportNamespace() {
        return exportNamespace;
    }

    /**
     * @param exportNamespace the exportNamespace to set
     */
    public void setExportNamespace(ArrayList<String> exportNamespace) {
        this.exportNamespace = exportNamespace;
    }

    /**
     * @return the canExportImport
     */
    public Boolean getCanExportImport() {
        return canExportImport;
    }

    /**
     * @param canExportImport the canExportImport to set
     */
    public void setCanExportImport(Boolean canExportImport) {
        this.canExportImport = canExportImport;
    }

}
