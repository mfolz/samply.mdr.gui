/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.gui;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;

import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.lib.AdapterDefinition;
import de.samply.mdr.lib.Constants.Language;
import de.samply.mdr.util.Mapper;
import de.samply.string.util.StringUtil;

/**
 * An element from the mdr with all definitions.
 * Shows a "prioritized" language.
 * @author paul
 *
 */
public class MDRElement implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * The underlaying element.
     */
    private final IdentifiedElement element;

    /**
     * The list of definitions for this element.
     */
    private List<AdapterDefinition> definitions;

    /**
     * The prioritized language. This attribute is used to show the proper language.
     */
    private Language priority;

    /**
     * Creates a new MDRelement for the given identified element and the given prioritized language.
     * @param element
     * @param priority
     */
    public MDRElement(IdentifiedElement element, Language priority) {
        this.element = element;
        this.priority = priority;
        this.definitions = Mapper.convert(element.getDefinitions());
    }

    public List<AdapterDefinition> getDefinitions() {
        return definitions;
    }

    public String getDefinition() {
        return getPrioritizedDefinition().getDefinition();
    }

    public String getDesignation() {
        return getPrioritizedDefinition().getDesignation();
    }

    /**
     * Returns the prioritized definition, if there is one. If there is none,
     * this method returns the english definition. If there is no english definition
     * it returns the first definition.
     * @return
     */
    public AdapterDefinition getPrioritizedDefinition() {
        for(AdapterDefinition d : definitions) {
            if(d.getLanguage() == priority) {
                return d;
            }
        }

        for(AdapterDefinition d : definitions) {
            if(d.getLanguage() == Language.EN) {
                return d;
            }
        }

        return definitions.get(0);
    }

    /**
     * Returns true if the element has been imported from another MDR.
     * @return
     */
    public Boolean getIsExternal() {
        return ! StringUtil.isEmpty(element.getElement().getExternalId());
    }

    /**
     * Returns the external label
     * @return
     */
    public String getExternalLabel() {
        String externalId = element.getElement().getExternalId();
        if(externalId.startsWith("cadsr")) {
            return "caDSR";
        } else {
            return "unknown";
        }
    }

    /**
     * Returns the outcome that will show the *original external* element
     * @return
     */
    public String getOutcome() {
        String externalId = element.getElement().getExternalId();
        if(externalId.startsWith("cadsr")) {
            try {
                URIBuilder builder = new URIBuilder(externalId);
                String publicId = "0";

                for(NameValuePair value : builder.getQueryParams()) {
                    if(value.getName().equals("publicId")) {
                        publicId = value.getValue();
                    }
                }
                return "/cadsr?publicId=" + publicId;
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return "unknown";
        }
    }

    public String getElementType() {
        return getElement().getElementType().toString().toLowerCase();
    }

    public IdentifiedElement getElement() {
        if(element == null) {
            System.out.println("ELEMENT IS NULL!");
        }
        return element;
    }

    public String getUrn() {
        return element.getScoped().toString();
    }

    public String getUrnId() {
        return getUrn().replace(':', '_');
    }

    @Override
    public int hashCode() {
        return getUrn().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof MDRElement)) {
            return false;
        }
        return ((MDRElement)obj).getUrn().equals(getUrn());
    }

    public Status getStatus() {
        return element.getScoped().getStatus();
    }

    public Boolean getReleased() {
        return getStatus() == Status.RELEASED;
    }

    public Boolean getDraft() {
        return getStatus() == Status.DRAFT;
    }

    public Boolean getOutdated() {
        return getStatus() == Status.OUTDATED;
    }

    public boolean firstDraft() {
        return getDraft() && getElement().getScoped().getVersion().equals("1");
    }

}
